module Main where

import Data.Char
import Data.List
import Debug.Trace
import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.String
import qualified Control.Applicative as CA
import Data.Maybe

day15 n = fmap (\l->catMaybes $ [(+y) . (*4000000) <$> merge (sort (f (trace (show y) y) l)) | y <- [0..n]]) . parse (many sensor) "-" . filter (/='\n')
  where
    merge [] = Nothing
    merge (m:ms) = merge' m ms
      where
        merge' :: (Int, Int) -> [(Int, Int)] -> Maybe Int
        merge' (x1, x2) [] = Nothing
        merge' (x1, x2) ((x3, x4):rest)
            | x3 - x2 > 1 = Just $ x2+1
            | otherwise = merge' (x1, max x2 x4) rest

    f :: Int -> [(Int, Int, Int, Int)] -> [_]
    f n [] = []
    f n (o@(sx, sy, _, _):os)
        | d <= 0 = f n os
        | otherwise = (sx-d, sx+d):f n os
      where d = distanceBeaconSensor o - abs (sy-n)

distanceBeaconSensor (sx, sy, bx, by) = abs(sx-bx) + abs(sy-by)

sensor = (,,,) <$ string "Sensor at x=" <*> int <* string ", y=" <*> int <* string ": closest beacon is at x=" <*> int  <* string ", y=" <*> int

int :: Parser Int
int = (*(-1)) <$ char '-' <*> int
    <|> read <$> many1 digit

-- part two
main = interact (show . day15 4000000)
