module Main where

import Data.Char
import Data.List
import qualified Data.Map as DM
import qualified Data.Set as DS

type Dist = DM.Map (Int, Int) Int

day12 inp = minimum [d | (c, d) <- DM.assocs $ find q (DM.insert end 0 dist), kaart DM.! c == 0]
  where
    dist :: Dist
    dist = DM.fromList [(c, 99999) | c <- DM.keys kaart]
    q = DM.keys kaart

    start,end :: (Int, Int)
    start = head [ c | (c, v) <- DM.assocs kaart', v == -14]
    end = head [ c | (c, v) <- DM.assocs kaart', v == -28]

    kaart :: DM.Map (Int, Int) Int
    kaart = DM.insert start 0 $ DM.insert end 25 kaart'

    kaart' :: DM.Map (Int, Int) Int
    kaart' = DM.fromList [ ((x, y), ord c - ord 'a') | (l, y) <- lines inp `zip` [0..], (c, x)<-l `zip` [0..]]

    find :: [(Int, Int)] -> Dist -> Dist
    find [] dist = dist
    find qs dist = find qs' $ foldl neighbour dist (neighbours u)
      where u:qs' = sortBy (\a b->DM.lookup a dist `compare` DM.lookup b dist) qs
            neighbour dist v
                | v `elem` qs' && kaart DM.! u - kaart DM.! v <= 1
                    = if alt < dist DM.! v
                        then DM.insert v alt dist
                        else dist
                | otherwise = dist
              where alt = dist DM.! u + 1

neighbours (x, y) = [(x+1, y), (x, y+1), (x-1, y), (x, y-1)]

-- part two
main = interact (show . day12)
