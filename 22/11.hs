{-# LANGUAGE BangPatterns #-}
module Main where

import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.String
import Data.Maybe
import Data.List
import qualified Data.IntMap.Strict as DM

data Monkey = Monkey
    { num :: Int
    , items :: [Int]
    , operation :: (Maybe Int, Int -> Int -> Int, Maybe Int)
    , dby :: Int
    , iftrue :: Int
    , iffalse :: Int
    , inspects :: Int}

day11 op nr = fmap (product . take 2 . reverse . sort . map inspects . DM.elems . nrounds nr . toMap) . parse (sepBy monkey newline) "-"

toMap ms = DM.fromList [(num m, m) | m <- ms]

nrounds :: Int -> DM.IntMap Monkey -> DM.IntMap Monkey
nrounds 0 !ms = ms
nrounds n !ms = nrounds (n-1) $ rnd ms

rnd :: DM.IntMap Monkey -> DM.IntMap Monkey
rnd !ms = foldl' (\ms i->inspect (ms DM.! i) ms) ms $ DM.keys ms
  where
    inspect :: Monkey -> DM.IntMap Monkey -> DM.IntMap Monkey
    inspect m@Monkey{items=[]} !ms = clear (num m) ms
    inspect m@Monkey{items=item:items} !ms
        = inspect (m { items=items }) $ throw newworry (if (newworry `mod` dby m) == 0 then iftrue m else iffalse m) ms
      where newworry = worry (dby m) item (operation m)

    throw item = DM.alter (fmap $ \m->m { items=items m ++ [item] })
    clear = DM.alter (fmap $ \m->m { items=[], inspects=inspects m + length (items m) })

    worry m old (l, op, r) = (fromMaybe old l `op` fromMaybe old r) `mod` supermodulo

    supermodulo = product $ map dby $ DM.elems ms

monkey = Monkey <$ string "Monkey" <* space <*> int <* char ':' <* newline
    <* many space <* string "Starting items: " <*> sepBy int (string ", ") <* newline
    <* many space <* string "Operation: new = " <*> exp <* newline
    <* many space <* string "Test: divisible by " <*> int <* newline
    <* many space <* string "If true: throw to monkey " <*> int <* newline
    <* many space <* string "If false: throw to monkey " <*> int <* newline
    <*> pure 0
  where exp = (,,) <$> oldornum <* space <*> ((+) <$ char '+' <|> (*) <$ char '*') <* space <*> oldornum
        oldornum = Nothing <$ string "old" <|> Just <$> int

int :: Parser Int
int = read <$> many digit

-- part one
--main = interact (show . day11 20)
-- part two
main = interact (show . day11 10000)
