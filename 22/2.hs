module Main where

import Data.List

day2 :: (String -> Integer) -> String -> Integer
day2 table = sum . map table . lines

one "A X" = 1 + 3
one "A Y" = 2 + 6
one "A Z" = 3 + 0
one "B X" = 1 + 0
one "B Y" = 2 + 3
one "B Z" = 3 + 6
one "C X" = 1 + 6
one "C Y" = 2 + 0
one "C Z" = 3 + 3
one _ = 0

two "A X" = 3 + 0
two "A Y" = 1 + 3
two "A Z" = 2 + 6
two "B X" = 1 + 0
two "B Y" = 2 + 3
two "B Z" = 3 + 6
two "C X" = 2 + 0
two "C Y" = 3 + 3
two "C Z" = 1 + 6
two _ = 0

-- part one
--main = interact (show . day2 one)
-- part two
main = interact (show . day2 two)
