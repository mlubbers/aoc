module Main where

import Data.List

day1 :: Int -> String -> Integer
day1 n = sum . foldl maxn (replicate n 0) . toSum 0 . lines
  where maxn es e = take n $ insertBy (flip compare) e es

toSum :: Integer -> [String] -> [Integer]
toSum acc [] = [acc]
toSum acc ("":xs) = acc:toSum 0 xs
toSum acc (x:xs) = toSum (acc + read x) xs

-- part one
--main = interact (show . day1 1)
-- part two
main = interact (show . day1 3)
