module Main where

import qualified Data.Set as DS

data State = S {vis :: DS.Set (Int, Int), knots :: [(Int, Int)], hd :: (Int, Int)}

day9 nknots = DS.size . vis . foldl step zero . map parseCmd . lines
  where zero = S DS.empty (replicate nknots (0, 0)) (0, 0)
        parseCmd l = let ([dir], ' ':num) = span (/=' ') l in (dir, read num)

step :: State -> (Char, Int) -> State
step s (d, n) = foldl (flip ($)) s $ replicate n step1
  where
    step1 :: State -> State
    step1 (S vis knots hd) =
        let hd'    = move d hd
            knots' = procKnots hd' knots
            vis'   = DS.insert (last knots') vis
        in S vis' knots' hd'
      where procKnots hd [] = []
            procKnots hd (k:knots) = let k' = follow k hd in k':procKnots k' knots

move :: Char -> (Int, Int) -> (Int, Int)
move 'U' (x, y) = (x, y+1)
move 'D' (x, y) = (x, y-1)
move 'R' (x, y) = (x+1, y)
move 'L' (x, y) = (x-1, y)

follow :: (Int, Int) -> (Int, Int) -> (Int, Int)
follow (tx, ty) (hx, hy)
    -- diagonal and y more than 1 distance
    | abs (hx-tx) == 1 && abs (hy-ty) > 1
        = (tx + signum (hx-tx), ty + (hy-ty) - signum (hy-ty))
    -- diagonal and x more than 1 distance
    | abs (hy-ty) == 1 && abs (hx-tx) > 1
        = (tx + (hx-tx) - signum (hx-tx), ty + signum (hy-ty))
    -- other cases, no or only in one dimension distance
    | otherwise = (tx + (hx-tx) - signum (hx-tx), ty + (hy-ty) - signum (hy-ty))

-- part one
--main = interact (show . day9 1)
-- part two
main = interact (show . day9 9)
