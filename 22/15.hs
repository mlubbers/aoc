module Main where

import Data.Char
import Data.List
import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.String
import qualified Control.Applicative as CA
import Data.Maybe

data List = List [List] | Num Int
  deriving (Show, Eq)

day15 :: Int -> String -> _
day15 n = fmap (merge . sort . f) . parse (many sensor) "-" . filter (/='\n')
  where
    merge :: [(Int, Int)] -> Int
    merge [] = 0
    merge (m:ms) = merge' m ms
      where
        merge' :: (Int, Int) -> [(Int, Int)] -> Int
        merge' (x1, x2) [] = x2-x1
        merge' (x1, x2) ((x3, x4):rest)
            | x3 > x2 = (x2-x1) + merge' (x3, x4) rest
            | otherwise = merge' (x1, max x2 x4) rest

    f :: [(Int, Int, Int, Int)] -> [_]
    f [] = []
    f (o@(sx, sy, _, _):os)
        | d <= 0 = f os
        | otherwise = (sx-d, sx+d):f os
      where d = distanceBeaconSensor o - abs (sy-n)

distanceBeaconSensor (sx, sy, bx, by) = abs(sx-bx) + abs(sy-by)

sensor = (,,,) <$ string "Sensor at x=" <*> int <* string ", y=" <*> int <* string ": closest beacon is at x=" <*> int  <* string ", y=" <*> int

int :: Parser Int
int = (*(-1)) <$ char '-' <*> int
    <|> read <$> many1 digit

-- part one
main = interact (show . day15 2000000)
