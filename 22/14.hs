module Main where

import qualified Data.Map as DM

day14 = foldr line DM.empty . lines


main = interact (show . day14)
