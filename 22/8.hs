module Main where

import Data.List
import qualified Data.Set as DS
import qualified Data.Map as DM
import Data.Char


day8 inp = DS.size $ DS.unions
        $  [visible (-1) r [(x, y) | x<-[0..maxx]] | (r, y) <- rows `zip` [0..]]
        ++ [visible (-1) c [(x, y) | y<-[0..maxy]] | (c, x) <- cols `zip` [0..]]
  where rows = map (map (\x->ord x - ord '0')) $ lines inp
        cols = transpose rows
        maxx = length rows - 1
        maxy = length (head rows) - 1

visible :: Int -> [Int] -> [(Int, Int)] -> DS.Set (Int, Int)
visible m cs xs = vis m cs xs `DS.union` vis m (reverse cs) (reverse xs)

vis m (c:cs) (x:xs)
    | c > m = DS.insert x $ vis c cs xs
    | otherwise = vis m cs xs
vis _ _ _ = DS.empty

-- part one
--main = interact (show . day8)
-- part two
main = interact (show . day82)

day82 inp = foldr (max . scenic dat) 0 $ DM.keys dat
  where
    dat = DM.fromList [((x, y), ord c - ord '0') | (l, y) <- lines inp `zip` [0..], (c, x) <- l `zip` [0..]]

    scenic :: DM.Map (Integer, Integer) Int -> (Integer, Integer) -> Int
    scenic m c@(x, y)
        = view (m DM.! c) [(x, y-i) | i<-[1..]] -- up
        * view (m DM.! c) [(x, y+i) | i<-[1..]] -- down
        * view (m DM.! c) [(x-i, y) | i<-[1..]] -- left
        * view (m DM.! c) [(x+i, y) | i<-[1..]] -- right
      where
        view :: Int -> [(Integer, Integer)] -> Int
        view mx (c:cs) = case m DM.!? c of
            Nothing -> 0 -- off the map
            Just h 
                | h >= mx -> 1 -- next tree is higher than previously seen
                | otherwise -> 1 + view mx cs
