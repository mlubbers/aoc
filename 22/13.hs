module Main where

import Data.Char
import Data.List
import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.String
import qualified Control.Applicative as CA
import Data.Maybe

data List = List [List] | Num Int
  deriving (Show, Eq)

day13 f = fmap f . parse listpairs "-" . filter (/='\n')

listpairs = many ((,) <$> list <*> list)
list =  List <$ char '[' <*> (list `sepBy` char ',') <* char ']'
    <|> Num . read <$> many1 digit

instance Ord List where
    compare (Num l) (Num r) = compare l r
    compare (List l) (List r) = compare l r
    compare (Num l) (List r) = compare (List [Num l]) (List r)
    compare (List l) (Num r) = compare (List l) (List [Num r])

-- part one
--main = interact (show . day13 one)
-- part two
main = interact (show . day13 two)

one :: [(List, List)] -> Integer
one = sum . map fst . filter ((==LT) . snd) . zip [1..] . map (uncurry compare)
two ls = product . map fst . filter (\x->snd x `elem` [d1,d2]) $ zip [1..] $ sort $ d1:d2:concat [[l,r] | (l, r) <- ls]
d1 = List [List [Num 2]]
d2 = List [List [Num 6]]
