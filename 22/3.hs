module Main where

import Data.List
import Data.Char

day3 = sum . map (prio . head . uncurry intersect . split) . lines
  where split l = splitAt (length l `div` 2) l

day32 = sum . group3 . lines
  where
    group3 (a:b:c:rest) = prio (head $ a `intersect` b `intersect` c):group3 rest
    group3 _ = []

prio c
    | isLower c = ord c - 97 + 1
    | isUpper c = ord c - 65 + 27

-- part one
--main = interact (show . day3)
-- part two
main = interact (show . day32)
