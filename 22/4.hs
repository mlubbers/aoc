module Main where

import Data.List
import Data.Char

day4 pred = length . filter (\(a, b)->pred a b || pred b a) . map parse . lines
  where
    parse :: [Char] -> ((Int, Int), (Int, Int))
    parse r0 =
        let (fro1, '-':r1) = span isDigit r0
            (to1, ',':r2) = span isDigit r1
            (fro2, '-':r3) = span isDigit r2
            (to2, _) = span isDigit r3
        in ((read fro1, read to1), (read fro2, read to2))

-- part one
--main = interact (show . day4 fullyContains)
-- part two
main = interact (show . day4 overlap)

fullyContains (a, b) (c, d) = a >= c && b <= d
overlap (a, b) (c, d) = a >= c && a <= d || b >= c && b <= d
