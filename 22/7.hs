{-# LANGUAGE FlexibleContexts #-}
module Main where

import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.String
import qualified Data.Map as DM
import Control.Monad.State
import Control.Monad.Reader
import Control.Monad.Writer
import qualified Data.List as DL

day7 fun = either show (fun . exec) . parse (many line) "-"
  where exec r = execWriter $ size $ Fork $ execState (runReaderT (execute r) []) DM.empty

data Cmd
    = Cd Target
    | Ls [Entry]
  deriving Show
data Target = Up | Root | Down String
  deriving Show
data Entry = Dir String | File Int String
  deriving Show
data FSEntry = Fork (DM.Map String FSEntry) | Leaf Int
  deriving Show

size :: MonadWriter [Int] m => FSEntry -> m Int
size (Leaf sz) = pure sz
size (Fork m) = do
    sz <- sum <$> mapM size (DM.elems m)
    tell [sz]
    pure sz

draw i c (Leaf size) = concat [replicate i '\t', "- ", c, " (file, size=", show size, ")\n"]
draw i c (Fork m) = concat $ replicate i '\t':"- ":c:" (dir)\n":[draw (i+1) k v | (k, v)<-DM.assocs m]

execute :: (MonadState (DM.Map String FSEntry) m, MonadReader [String] m) => [Cmd] -> m ()
execute (Cd Up:rest) = local init $ execute rest
execute (Cd Root:rest) = local (const []) $ execute rest
execute (Cd (Down d):rest) = do
    pwd <- ask
    modify (insert pwd (Dir d))
    local (++[d]) $ execute rest
execute (Ls entries:rest) = do
    pwd <- ask
    modify (\s->foldr (insert pwd) s entries)
    execute rest
execute [] = pure ()

insert :: [String] -> Entry -> DM.Map String FSEntry -> DM.Map String FSEntry
insert [] e m = case e of
    File size name -> DM.insert name (Leaf size) m
    Dir s -> DM.alter (maybe (Just $ Fork DM.empty) (const Nothing)) s m
insert (c:cs) e m = DM.alter (Just . f) c m
  where f Nothing = Fork $ insert cs e DM.empty
        f (Just (Fork m)) = Fork $ insert cs e m

line :: Parser Cmd
line = char '$' *> char ' ' *> cmd

cmd :: Parser Cmd
cmd = Cd <$ string "cd" <* space <*> target <* endOfLine
    <|> Ls <$ string "ls" <* endOfLine <*> many (entry <* endOfLine)

target = Up <$ string ".." <|> Root <$ string "/" <|> Down <$> many notend

entry :: Parser Entry
entry = Dir <$ string "dir" <* space <*> many notend
    <|> File <$> (read <$> many digit) <* space <*> many notend

notend = satisfy $ \c->c /= '\n' && c /= '$'

-- part one
--main = interact (day7 (show . sum . filter (100000>=)))
-- part two
main = interact (day7 (show . two))
  where
    two s =
        let root:rest = reverse $ DL.sort s
            tofree = 30000000 - (70000000 - root)
        in last $ filter (tofree<) (root:rest)
