module Main where

import qualified Data.Set as DS
import Data.List.Split

one = show . sigstrength [20,60,100,140,180,220]
  where
    sigstrength :: [Int] -> [(Int, Int)] -> Int
    sigstrength [] _ = 0
    sigstrength (x:xs) ((i, y):ys)
        | x == i = y*x + sigstrength xs ys
        | otherwise = sigstrength (x:xs) ys

two = unlines . chunksOf 40 . map draw
  where 
    draw (ncycle,x) = if nc >= x && nc <= x + 2 then '#' else '.'
      where nc = ncycle `mod` 40

day10 f = f . zip [1..] . cmd 1 . map (span (/=' ')) . lines
  where
    cmd :: Int -> [(String, String)] -> [Int]
    cmd x (("noop", _):rest) = x:cmd x rest
    cmd x (("addx", ' ':num):rest) = x:x:cmd (x + read num) rest
    cmd x [] = []

-- part one
--main = interact (show . day10 one)
-- part two
main = interact (day10 two)
