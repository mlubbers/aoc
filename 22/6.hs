module Main where

import Data.List

day6 len = fmap (+len) . elemIndex True . map ((==len) . length . nub . take len) . tails

-- part one
--main = interact (show . day6 4)
-- part two
main = interact (show . day6 14)
