module Main where

import Data.Char
import Data.List
import Data.Bifunctor

day5 op = map head . uncurry (foldl apply) . bimap parseStacks (map parseOp . drop 1) . break null . lines
  where
    parseStacks :: [[Char]] -> [[Char]]
    parseStacks = map (dropWhile isSpace) . transpose . map (dropWhileEnd isDigit . group3)
      where group3 (_:y:_:_:rest) = y:group3 rest
            group3 [_,y,_] = [y]

    parseOp :: [Char] -> (Int, Int, Int)
    parseOp ('m':'o':'v':'e':' ':r0) =
        let (num, ' ':'f':'r':'o':'m':' ':r1) = span isDigit r0
            (fro, ' ':'t':'o':' ':r2) = span isDigit r1
            (to, _) = span isDigit r2
        in (read num, read fro, read to)

    apply :: [[Char]] -> (Int, Int, Int) -> [[Char]]
    apply stacks (n, fro, to) = push to $ pop fro stacks
      where
        pop :: Int -> [[Char]] -> ([Char], [[Char]])
        pop 1 (s:stacks) = let (x, s') = splitAt n s in (op x, s':stacks)
        pop n (s:stacks) = let (x, stacks') = pop (n-1) stacks in (x, s:stacks')

        push :: Int -> ([Char], [[Char]]) -> [[Char]]
        push 1 (x, s:stacks) = (x++s):stacks
        push n (x, s:stacks) = s:push (n-1) (x, stacks)

-- part one
--main = interact (show . day5 reverse)
-- part two
main = interact (show . day5 id)
