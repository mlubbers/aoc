#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define max(a, b) ((a)>(b) ? (a) : (b))
#define min(a, b) ((a)<(b) ? (a) : (b))

struct cube { long x1, x2, y1, y2, z1, z2; struct cube *next; };

struct cube *parse_line(bool *on)
{
	struct cube *r = malloc(sizeof(struct cube));
	char *buf = NULL;
	size_t len;
	if (getline(&buf, &len, stdin) == -1)
		return NULL;
	if (strncmp(buf, "on", 2) == 0) {
		*on = true;
		buf += 3;
	} else if (strncmp(buf, "off", 3) == 0) {
		*on = false;
		buf += 4;
	} else {
		return NULL;
	}
	sscanf(buf, "x=%li..%li,y=%li..%li,z=%li..%li",
		&r->x1, &r->x2,
		&r->y1, &r->y2,
		&r->z1, &r->z2);
	r->next = NULL;
	return r;
}

struct cube *merge(struct cube *a, bool on, struct cube *b)
{
	//it didn't overlap with any cubes
	if (a == NULL)
		// if its on, ignore, otherwise add
		return on ? b : NULL;
	int xo1 = max(a->x1, b->x1);
	int xo2 = min(a->x2, b->x2);
	int yo1 = max(a->y1, b->y1);
	int yo2 = min(a->y2, b->y2);
	int zo1 = max(a->z1, b->z1);
	int zo2 = min(a->z2, b->z2);
	//they don't overlap
	if (xo2-xo1 < 0 || yo2-yo1 < 0 || zo2-zo1 < 0) {
		//continue with the next cube
		a->next = merge(a->next, on, b);
		return a;
	//they overlap
	} else {
		int xl1 = min(a->x1, b->x1);
		int xl2 = max(a->x1, b->x1);
		int yl1 = min(a->y1, b->y1);
		int yl2 = max(a->y1, b->y1);
		int zl1 = min(a->z1, b->z1);
		int zl2 = max(a->z1, b->z1);
		int xr1 = min(a->x2, b->x2);
		int xr2 = max(a->x2, b->x2);
		int yr1 = min(a->y2, b->y2);
		int yr2 = max(a->y2, b->y2);
		int zr1 = min(a->z2, b->z2);
		int zr2 = max(a->z2, b->z2);
		
		return a;
	}
}

unsigned long volume(struct cube c)
{
	return (c.x2-c.x1) * (c.y2-c.y1) * (c.z2-c.z1);
}

int main()
{
	bool on = false;
	struct cube *c = parse_line(&on);
	struct cube *n;
	while ((n = parse_line(&on)) != NULL) {
		c = merge(c, on, n);
	}
}
