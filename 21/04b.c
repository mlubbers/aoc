#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

struct board {
	int numbers[5][5];
};

int parse_draw(int draw[])
{
	char buf[1000];
	if (fgets(buf, 1000, stdin) == NULL)
		exit(1);
	char *tok;
	int i = 0;
	tok = strtok(buf, ",");
	while(tok != NULL) {
		draw[i++] = atoi(tok);
		tok = strtok(NULL, ",");
	}
	return i;
}

bool board_wins(struct board board)
{
	for (int row = 0; row<5; row++) {
		bool rowwin = true, colwin = true;
		for (int col = 0; col<5 && (rowwin || colwin); col++) {
			rowwin &= board.numbers[row][col] == -1;
			colwin &= board.numbers[col][row] == -1;
		}
		if (rowwin || colwin)
			return true;
	}
	return false;
}

int parse_boards(struct board boards[])
{
	int i = 0;
	while(getchar() == '\n') {
		for (int row = 0; row<5; row++) {
			for (int col = 0; col<5; col++) {
				char buf[3] = {getchar(), getchar(), getchar()};
				boards[i].numbers[row][col] = atoi(buf);
			}
		}
		i++;
	}
	return i;
}

int main(void)
{
	int draws[1000];
	int ndraws = parse_draw(draws);

	struct board boards[1000];
	int nboards = parse_boards(boards);

	int draw;
	for (draw = 0; draw < ndraws && nboards > 0; draw++) {
		//Mark boards
		for (int board = 0; board < nboards; board++)
			for (int row = 0; row<5; row++)
				for (int col = 0; col<5; col++)
					if (boards[board].numbers[row][col] == draws[draw])
						boards[board].numbers[row][col] = -1;
		//Check winners
		for (int board = 0; board < nboards; ) {
			if (board_wins(boards[board])) {
				//Remove winner
				nboards--;
				for (int i = board; i<nboards; i++)
					boards[i] = boards[i+1];
			} else {
				board++;
			}
		}
	}

	//Calculate sum of unmarked numbers
	int sum = 0;
	for (int row = 0; row<5; row++)
		for (int col = 0; col<5; col++)
			if (boards[0].numbers[row][col] != -1)
				sum += boards[0].numbers[row][col];

	printf("%d\n", sum*draws[draw-1]);
}
