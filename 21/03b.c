#include <stdio.h>
#include <stdbool.h>

#define bitread(value, bit) (((value) >> (bit)) & 0x01)

int incremental_search(unsigned numbers[], int len, int bits, bool inv)
{
	//Copy for the initial search
	int cur[len];
	int curlen;
	for (curlen = 0; curlen<len; curlen++)
		cur[curlen] = numbers[curlen];

	for (int bit = bits-1; bit >= 0 || curlen > 1; bit--) {
		//Determine 0 1 distribution
		int dist = 0;
		for (int i = 0; i<curlen; i++)
			dist += bitread(cur[i], bit) == 0 ? -1 : 1;

		dist = dist >= 0;
		dist = inv ? 1-dist : dist;

		//Filter out numbers
		int newcurlen = 0;
		for (int i = 0; i<curlen; i++)
			if (bitread(cur[i], bit) == dist)
				cur[newcurlen++] = cur[i];
		curlen = newcurlen;
	}
	return cur[0];
}

int main(void)
{
	unsigned numbers[2000] = {0};
	int c = 0, y = 0, maxbits = 0, bits = 0;
	while ((c = getchar()) != EOF) {
		if (c == '\n') {
			maxbits = bits > maxbits ? bits : maxbits;
			bits = 0;
			y++;
		} else {
			numbers[y] *= 2;
			if (c == '1')
				numbers[y]++;
		}
	}

	int oxygen = incremental_search(numbers, y, maxbits, false);
	int co2 = incremental_search(numbers, y, maxbits, true);

	printf("%d\n", oxygen*co2);
	return 0;
}
