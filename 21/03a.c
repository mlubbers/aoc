#include <stdio.h>

int main(void)
{
	int bits[100] = {0};
	int i = 0, maxi = 0, c;
	while ((c = getchar()) != EOF) {
		if (c == '0')
			bits[i++] -= 1;
		else if (c == '1')
			bits[i++] += 1;
		else if (c == '\n')
			i = 0;
		maxi = i > maxi ? i : maxi;
	}

	int gamma = 0, epsilon = 0;
	for (int j = 0; j<maxi; j++) {
		gamma *= 2;
		epsilon *= 2;
		if (bits[j] > 0)
			gamma++;
		else
			epsilon++;
	}
	printf("%d\n", gamma*epsilon);
	return 0;
}
