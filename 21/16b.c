#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

char *hex2bin[] =
	{ ['0'] = "0000", ['1'] = "0001", ['2'] = "0010", ['3'] = "0011"
	, ['4'] = "0100", ['5'] = "0101", ['6'] = "0110", ['7'] = "0111"
	, ['8'] = "1000", ['9'] = "1001", ['A'] = "1010", ['B'] = "1011"
	, ['C'] = "1100", ['D'] = "1101", ['E'] = "1110", ['F'] = "1111"
	, ['\n'] = "0000"};

struct stream { int pos; char *buf; };

int next(struct stream *f)
{
	if (*f->buf == '\0')
		f->buf = hex2bin[getchar()];
	int r = *(f->buf++) == '1' ? 1 : 0;
	f->pos++;
	return r;
}

unsigned long bin2int(struct stream *f, int n)
{
	unsigned long r = 0;
	for (int i = 0; i<n; i++)
		r = 2*r+next(f);
	return r;
}

unsigned long parse_packet(struct stream *f)
{
	int packetversion  __attribute__((unused)) = bin2int(f, 3);
	int packettype = bin2int(f, 3);
	unsigned long r = 0;

	//literal
	if (packettype == 4) {
		while (next(f) == 1)
			r = r*16+bin2int(f, 4);
		r = r*16+bin2int(f, 4);
	//operator
	} else {
		int lengthtypeid = next(f);
		unsigned long packets[100] = {0};
		int npackets = 0;

		//number of bits
		if (lengthtypeid == 0) {
			int lengthsubpackets = bin2int(f, 15);
			int oldpos = f->pos;
			while (f->pos - oldpos < lengthsubpackets)
				packets[npackets++] = parse_packet(f);
		//number of packets
		} else {
			npackets = bin2int(f, 11);
			for (int i = 0; i<npackets; i++)
				packets[i] = parse_packet(f);
		}

		if (packettype == 0) {        //sum
			for (int i = 0; i<npackets; i++)
				r += packets[i];
		} else if (packettype == 1) { //product
			r = 1;
			for (int i = 0; i<npackets; i++)
				r *= packets[i];
		} else if (packettype == 2) { //minimum
			r = ULONG_MAX;
			for (int i = 0; i<npackets; i++)
				r = packets[i] < r ? packets[i] : r;
		} else if (packettype == 3) { //maximum
			for (int i = 0; i<npackets; i++)
				r = packets[i] > r ? packets[i] : r;
		} else if (packettype == 5) { //greater than
			r = packets[0] > packets[1];
		} else if (packettype == 6) { //less than
			r = packets[0] < packets[1];
		} else if (packettype == 7) { //equal to
			r = packets[0] == packets[1];
		}
	}
	return r;
}

int main()
{
	struct stream f = {.pos=0, .buf=""};
	printf("%lu\n", parse_packet(&f));
}
