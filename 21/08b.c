#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdbool.h>
#include <ctype.h>

#define INPUT_SIZE 10
#define OUTPUT_SIZE 4

//  000
// 1   2
// 1   2
//  333
// 4   5
// 4   5
//  666

void chars_keep(char *s, char *ks)
{
	int i = 0;
	int len = strlen(s);
	for (int j = 0; j<len; j++)
		if (strchr(ks, s[j]) != NULL)
			s[i++] = s[j];

	s[i] = '\0';
}

void chars_remove(char *s, char *rs)
{
	int i = 0;
	int len = strlen(s);
	for (int j = 0; j<len; j++)
		if (strchr(rs, s[j]) == NULL)
			s[i++] = s[j];
	s[i] = '\0';
}

int char_cmp(const void *l, const void *r)
{
	return *(const char *)l-*(const char *)r;
}

int calculate(char input[INPUT_SIZE][8], char output[OUTPUT_SIZE][8])
{
	char mapping[7][8] = {"abcdefg" ,"abcdefg" ,"abcdefg" ,"abcdefg" ,"abcdefg" ,"abcdefg" ,"abcdefg"};
	// find mapping
	for (int i = 0; i<INPUT_SIZE; i++) {
		switch(strlen(input[i])) {
		// 1
		case 2:
			chars_remove(mapping[0], input[i]);
			chars_remove(mapping[1], input[i]);
			chars_keep(mapping[2], input[i]);
			chars_remove(mapping[3], input[i]);
			chars_remove(mapping[4], input[i]);
			chars_keep(mapping[5], input[i]);
			chars_remove(mapping[6], input[i]);
			break;
		// 7
		case 3:
			chars_keep(mapping[0], input[i]);
			chars_remove(mapping[1], input[i]);
			chars_keep(mapping[2], input[i]);
			chars_remove(mapping[3], input[i]);
			chars_remove(mapping[4], input[i]);
			chars_keep(mapping[5], input[i]);
			chars_remove(mapping[6], input[i]);
			break;
		// 4
		case 4:
			chars_remove(mapping[0], input[i]);
			chars_keep(mapping[1], input[i]);
			chars_keep(mapping[2], input[i]);
			chars_keep(mapping[3], input[i]);
			chars_remove(mapping[4], input[i]);
			chars_keep(mapping[5], input[i]);
			chars_remove(mapping[6], input[i]);
			break;
		// 2, 3 or 5
		case 5:
			//No shared empties
			//keep 0 3 6
			chars_keep(mapping[0], input[i]);
			chars_keep(mapping[3], input[i]);
			chars_keep(mapping[6], input[i]);
			break;
		// 0, 6 or 9
		case 6:
			//No shared empties
			//keep 0156
			chars_keep(mapping[0], input[i]);
			chars_keep(mapping[1], input[i]);
			chars_keep(mapping[5], input[i]);
			chars_keep(mapping[6], input[i]);
			break;
		// 8
		case 7:
			//No empties
			//keep 0123456
			chars_keep(mapping[0], input[i]);
			chars_keep(mapping[1], input[i]);
			chars_keep(mapping[2], input[i]);
			chars_keep(mapping[3], input[i]);
			chars_keep(mapping[4], input[i]);
			chars_keep(mapping[5], input[i]);
			chars_keep(mapping[6], input[i]);
			break;
		}
	}
	for (int i = 0; i<7; i++)
		if (strlen(mapping[i]) == 1)
			for (int j = 0; j<7; j++)
				if (i != j)
					chars_remove(mapping[j], mapping[i]);

	//Apply mapping
	int r = 0;
	for (int i = 0; i<OUTPUT_SIZE; i++) {
		for (char *p = &output[i][0]; *p != '\0'; p++) {
			for (int j = 0; j<7; j++) {
				if (mapping[j][0] == *p) {
					*p = 'a'+j;
					break;
				}
			}
		}
		qsort(output[i], strlen(output[i]), 1, &char_cmp);
		r*=10;
		if      (strcmp(output[i], "abcefg") == 0)  ;
		else if (strcmp(output[i], "cf") == 0)      r += 1;
		else if (strcmp(output[i], "acdeg") == 0)   r += 2;
		else if (strcmp(output[i], "acdfg") == 0)   r += 3;
		else if (strcmp(output[i], "bcdf") == 0)    r += 4;
		else if (strcmp(output[i], "abdfg") == 0)   r += 5;
		else if (strcmp(output[i], "abdefg") == 0)  r += 6;
		else if (strcmp(output[i], "acf") == 0)     r += 7;
		else if (strcmp(output[i], "abcdefg") == 0) r += 8;
		else if (strcmp(output[i], "abcdfg") == 0)  r += 9;
		else
			printf("unknown sequence: %s\n", output[i]);
	}

	return r;
}

int main()
{
	char *buf = NULL;
	size_t len = 0;
	int r = 0;

	while (getline(&buf, &len, stdin) != -1) {
		char input[INPUT_SIZE][8];
		char output[OUTPUT_SIZE][8];
		int i = 0;
		char *p = strtok(buf, " ");
		bool inoutput = false;
		strcpy(input[i++], p);
		while ((p = strtok(NULL, " ")) != NULL) {
			if (strcmp(p, "|") == 0) {
				inoutput = true;
			} else if (inoutput) {
				if (p[strlen(p)-1] == '\n')
					p[strlen(p)-1] = '\0';
				strcpy(output[(i++ % INPUT_SIZE)], p);
			} else {
				strcpy(input[i++], p);
			}
		}

		r += calculate(input, output);

	}
	printf("%d\n", r);
}
