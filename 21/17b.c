#include <stdio.h>
#include <stdbool.h>

bool is_in(int dx, int dy, int x1, int x2, int y1, int y2)
{
	int x = 0, y = 0;
	while (y >= y1 && x <= x2) {
		x+=dx;
		y+=dy;
		dx = dx == 0 ? 0 : dx-1;
		dy--;
		if (x >= x1 && x <= x2 && y >= y1 && y <= y2)
			return true;
	}
	return false;
}

int main()
{
	int x1, x2, y1, y2, n = 0;
	scanf("target area: x=%d..%d, y=%d..%d\n", &x1, &x2, &y1, &y2);
	for (int dy = x2; dy >= y1; dy--)
		for (int dx = 0; dx <= x2; dx++)
			if (is_in(dx, dy, x1, x2, y1, y2))
				n++;
	printf("%d\n", n);
}
