#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

struct point { uint8_t x; uint8_t y; };
bool visited[102][102] = {0};
int grid[102][102];

int basin(int x, int y)
{
	struct point stack[100];

	int sp = 0;
	int size = 0;
	stack[sp++] = (struct point){.x=x, .y=y};

	struct point p;
	memset(&p, 0, sizeof p);
	while(--sp >= 0) {
		p = stack[sp];
		if (visited[p.y][p.x])
			continue;
		visited[p.y][p.x] = true;

		if (grid[p.y][p.x] == 9)
			continue;
		size++;

		if (!visited[p.y][p.x+1])
			stack[sp++] = (struct point){.x=p.x+1, .y=p.y};
		if (!visited[p.y][p.x-1])
			stack[sp++] = (struct point){.x=p.x-1, .y=p.y};
		if (!visited[p.y+1][p.x])
			stack[sp++] = (struct point){.x=p.x, .y=p.y+1};
		if (!visited[p.y-1][p.x])
			stack[sp++] = (struct point){.x=p.x, .y=p.y-1};
	}
	return size;
}

int main()
{
	for (int i = 0; i<102; i++)
		for (int j = 0; j<102; j++)
			grid[i][j] = 9;
	int x = 1, y = 1;
	int maxx = 1;
	int c = 0;
	while((c = getchar()) != EOF) {
		if (c == '\n') {
			y++;
			x = 1;
		} else {
			maxx = x > maxx ? x : maxx;
			grid[y][x++] = c-'0';
		}
	}

	int tl[3] = {0};
	for (int cy = 1; cy<y; cy++) {
		for (int cx = 1; cx<maxx+1; cx++) {
			if (!visited[cy][cx]) {
				int s = basin(cx, cy);
				if (s >= tl[0]) {
					tl[2] = tl[1];
					tl[1] = tl[0];
					tl[0] = s;
				} else if (s >= tl[1]) {
					tl[2] = tl[1];
					tl[1] = s;
				} else if (s > tl[2]) {
					tl[2] = s;
				}
			}
		}
	}
	printf("%d\n", tl[0]*tl[1]*tl[2]);
}
