#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

int parse_draw(int draw[])
{
	char buf[1000];
	if (fgets(buf, 1000, stdin) == NULL)
		exit(1);
	char *tok;
	int i = 0;
	tok = strtok(buf, ",");
	while(tok != NULL) {
		draw[i++] = atoi(tok);
		tok = strtok(NULL, ",");
	}
	return i;
}

int parse_boards(int boards[][5][5])
{
	int i = 0;
	while(getchar() == '\n') {
		for (int row = 0; row<5; row++) {
			for (int col = 0; col<5; col++) {
				char buf[3] = {getchar(), getchar(), getchar()};
				boards[i][row][col] = atoi(buf);
			}
		}
		i++;
	}
	return i;
}

bool board_wins(int board[5][5])
{
	for (int row = 0; row<5; row++) {
		bool rowwin = true, colwin = true;
		for (int col = 0; col<5 && (rowwin || colwin); col++) {
			rowwin &= board[row][col] == -1;
			colwin &= board[col][row] == -1;
		}
		if (rowwin || colwin)
			return true;
	}
	return false;
}

int main(void)
{
	int draws[1000];
	int ndraws = parse_draw(draws);

	int boards[1000][5][5];
	int nboards = parse_boards(boards);

	int winner = -1;
	int draw;
	for (draw = 0; draw < ndraws && winner == -1; draw++) {
		//Mark boards
		for (int board = 0; board < nboards; board++)
			for (int row = 0; row<5; row++)
				for (int col = 0; col<5; col++)
					if (boards[board][row][col] == draws[draw])
						boards[board][row][col] = -1;
		//Check winners
		for (int board = 0; board < nboards && winner == -1; board++)
			if (board_wins(boards[board]))
				winner = board;
	}

	//Calculate sum of unmarked numbers
	int sum = 0;
	for (int row = 0; row<5; row++)
		for (int col = 0; col<5; col++)
			if (boards[winner][row][col] != -1)
				sum += boards[winner][row][col];

	printf("%d\n", sum*draws[draw-1]);
}
