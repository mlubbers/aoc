#include <stdio.h>

void flash(char grid[10][10], int *flashes, int x, int y)
{
	//already flashed or not charged up enough
	if (grid[y][x] == 0 || grid[y][x] < 10)
		return;
	(*flashes)++;
	grid[y][x] = 0;
	for (int dy = -1; dy<=1; dy++)
		for (int dx = -1; dx<=1; dx++)
			if ( (dx != 0 || dy != 0) &&
					x+dx >= 0 && x+dx < 10 &&
					y+dy >= 0 && y+dy < 10) {
				if (grid[y+dy][x+dx] != 0)
					grid[y+dy][x+dx]++;
				flash(grid, flashes, x+dx, y+dy);
			}
}

int main()
{
	int x = 0, y = 0;
	int c;
	char grid[10][10];
	while( (c = getchar()) != EOF) {
		if (c == '\n') {
			y++;
			x = 0;
		} else {
			grid[y][x++] = c-'0';
		}
	}

	int flashes = 0;
	int gen = 0;
	while (flashes != 100) {
		flashes = 0;
		gen++;
		for (int y = 0; y<10; y++)
			for (int x = 0; x<10; x++)
				grid[y][x]++;
		for (int y = 0; y<10; y++)
			for (int x = 0; x<10; x++)
				flash(grid, &flashes, x, y);
	}
	printf("%d\n", gen);
}
