#include <stdio.h>
#include <stdbool.h>
#include <string.h>

struct point {int x; int y; int z; };
struct scanner {
	int num;
	int npoints;
	struct point points[100];
};

bool parse_scanner(struct scanner *s)
{
	char *b = NULL;
	size_t len;
	if (-1 == getline(&b, &len, stdin))
		return false;
	sscanf(b, "--- scanner %d ---\n", &s->num);
	while (getline(&b, &len, stdin) != -1 && strcmp(b, "\n") != 0) {
		sscanf(b, "%d,%d,%d\n"
			, &s->points[s->npoints].x
			, &s->points[s->npoints].y
			, &s->points[s->npoints].z);
		s->npoints++;
	}
	return true;
}

struct point orient(struct point p, struct point r, int orientation)
{
	if (orientation == 0)
		return (struct point){.x=r.x*p.x, .y=r.y*p.y, .z=r.z*p.z};
	else if (orientation == 1)
		return (struct point){.x=r.x*p.x, .y=r.y*p.z, .z=r.z*p.y};
	else if (orientation == 2)
		return (struct point){.x=r.x*p.y, .y=r.y*p.x, .z=r.z*p.z};
	else if (orientation == 3)
		return (struct point){.x=r.x*p.y, .y=r.y*p.z, .z=r.z*p.x};
	else if (orientation == 4)
		return (struct point){.x=r.x*p.z, .y=r.y*p.x, .z=r.z*p.y};
	else if (orientation == 5)
		return (struct point){.x=r.x*p.z, .y=r.y*p.y, .z=r.z*p.x};
	else
		fprintf(stderr, "unknown orientation\n");
}
struct point rotations[] =
	{ {-1, -1, -1}
	, {-1, -1,  1}
	, {-1,  1, -1}
	, {-1,  1,  1}
	, { 1, -1, -1}
	, { 1, -1,  1}
	, { 1,  1, -1}
	, { 1,  1,  1}
	};

void match_scanners(struct scanner *l, struct scanner *r, int rot, int or)
{
	for (int dx = -1000; dx<1000; dx++) {
		for (int dy = -1000; dy<1000; dy++) {
			for (int dz = -1000; dz<1000; dz++) {
				

			}
		}
	}
}

int main()
{
	int nscanner = 0;
	struct scanner scanners[25];
	while(parse_scanner(&scanners[nscanner]))
		nscanner++;

	for (int i = 0; i<nscanner; i++) {
		for (int j = 0; j<nscanner; j++) {
			if (i == j)
				continue;
			for (int rot = 0; rot<8; rot++) {
				for (int or = 0; or<6; or++) {
					match_scanners(&scanners[i], &scanners[j], rot, or);
				}
			}
		}
	}

}
