#include <stdio.h>
#include <limits.h>
#include <uthash.h>

#define MFAC 5

struct point { int x; int y; };
#define pnt(px, py) ((struct point){.x=(px), .y=(py)})
struct q { struct point key; UT_hash_handle hh; };
int dist[100*MFAC][100*MFAC];

int grid[100][100] = {0};
int maxx = 0;
int maxy = 0;

int get_grid(int x, int y)
{
	int r = grid[y%maxy][x%maxx] + y/maxy + x/maxx;
	if (r > 9)
		return r % 9;
	return r;
}

int qcmp(const void *l, const void *r)
{
	const struct q *lp = (const struct q *)l;
	const struct q *rp = (const struct q *)r;
	return dist[lp->key.y][lp->key.x]-dist[rp->key.y][rp->key.x];
}

int main()
{
	int c, x = 0;;
	while ( (c = getchar()) != EOF) {
		if (c == '\n') {
			maxx = x;
			x = 0;
			maxy++;
		} else {
			grid[maxy][x++] = c-'0';
		}
	}

	struct q *q = NULL;

	for (int y = 0; y<maxy*MFAC; y++) {
		for (int x = 0; x<maxx*MFAC; x++) {
			dist[y][x] = INT_MAX;
			struct q *i = malloc(sizeof(struct q));
			i->key = pnt(x, y);
			HASH_ADD(hh, q, key, sizeof(struct point), i);
		}
	}
	dist[0][0] = 0;
	HASH_SRT(hh, q, qcmp);

	while (q != NULL) {
		struct point u = q->key;
		HASH_DELETE(hh, q, q);
		for (int dy = -1; dy <= 1; dy++) {
			for (int dx = -1; dx <= 1; dx++) {
				//Skip diagonal neigbours
				if (abs(dx) == abs(dy))
					continue;
				//check if v is in Q
				struct q *v;
				HASH_FIND(hh, q, &pnt(u.x+dx, u.y+dy), sizeof(struct point), v);
				if (v == NULL)
					continue;
				//Update dist if necessary
				int alt = dist[u.y][u.x] + get_grid(v->key.x, v->key.y);
				if (alt < dist[v->key.y][v->key.x]) {
					dist[v->key.y][v->key.x] = alt;
					//Reinsert with in correct position
					HASH_DELETE(hh, q, v);
					HASH_ADD_INORDER(hh, q, key, sizeof(struct point), v, qcmp);
				}
			}
		}
	}

	printf("%d\n", dist[maxy*MFAC-1][maxx*MFAC-1]);
}
