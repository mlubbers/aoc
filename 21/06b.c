#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gmp.h>

int main()
{
	char *buf = NULL;
	size_t len = 0;
	mpz_t fish[9];
	for (int i = 0; i<9; i++)
		mpz_init_set_ui(fish[i], 0);

	getline(&buf, &len, stdin);
	char *p = strtok(buf, ",");
	int n = atoi(p);
	mpz_add_ui(fish[n], fish[n], 1);
	while ((p = strtok(NULL, ",")) != NULL) {
		n = atoi(p);
		mpz_add_ui(fish[n], fish[n], 1);
	}

	for (long day = 0; day<256; day++) {
		n = (day+7) % 9;
		mpz_add(fish[n], fish[n], fish[day%9]);
	}

	for (long i = 1; i<=8; i++)
		mpz_add(fish[0], fish[0], fish[i]);
	mpz_out_str(stdout, 10, fish[0]);
	putchar('\n');
}
