#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int main()
{
	char *buf = NULL;
	size_t len = 0;
	int r = 0;

	while (getline(&buf, &len, stdin) != -1) {
		char *p = strtok(buf, " ");
		bool inoutput = false;
		while ((p = strtok(NULL, " ")) != NULL) {
			if (strcmp(p, "|") == 0)
				inoutput = true;
			else if (inoutput) {
				int l = strlen(p);
				if (p[l-1] == '\n')
					p[(l--)-1] = '\0';
				if (l == 2 || l == 3 || l == 4 || l == 7)
					r++;
			}
		}
	}
	printf("%d\n", r);
}
