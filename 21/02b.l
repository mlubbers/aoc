%option noyywrap
%option noinput
%option nounput
%{
#include <stdio.h>
int x = 0, z = 0, aim = 0;
%}

%%
forward\ [0-9] {
		int num = atoi(yytext+sizeof("forward"));
		x += num;
		z += aim*num;
	}
up\ [0-9]      aim -= atoi(yytext+sizeof("up"));
down\ [0-9]    aim += atoi(yytext+sizeof("down"));
\n	;
%%

int main (void)
{
	yylex();
	printf("%d\n", x*z);
	return 0;
}
