%option noyywrap
%option noinput
%option nounput
%{
#include <stdio.h>
int x = 0, z = 0;
%}

%%
forward\ [0-9] x += atoi(yytext+sizeof("forward"));
up\ [0-9]      z -= atoi(yytext+sizeof("up"));
down\ [0-9]    z += atoi(yytext+sizeof("down"));
\n	;
%%

int main (void)
{
	yylex();
	printf("%d\n", x*z);
	return 0;
}
