#include <stdio.h>

#include <uthash.h>

struct point {int x; int y; int gen; };
struct entry {
	struct point p;
	int c;
	UT_hash_handle hh;
};

int key[512] = {0};

void add_point(struct entry **board, int x, int y, int gen, int c)
{
	struct entry *e = calloc(1, sizeof(struct entry));
	e->p = (struct point){.x=x, .y=y, .gen=gen};
	e->c = c;
	HASH_ADD(hh, *board, p, sizeof(struct point), e);
}

struct entry *get_entry(struct entry **board, int x, int y, int gen)
{
	struct entry *e;
	struct point p;
	memset(&p, 0, sizeof(p));
	p.x = x;
	p.y = y;
	p.gen = gen;
	HASH_FIND(hh, *board, &p, sizeof(struct point), e);
	return e;
}

void del_point(struct entry **board, int x, int y, int gen)
{
	struct entry *e = get_entry(board, x, y, gen);
	if (e != NULL)
		HASH_DEL(*board, e);
}

int get_point(struct entry **board, int x, int y, int gen)
{
	struct entry *e = get_entry(board, x, y, gen);
	if (e == NULL)
		return key[0] == 1 ? gen % 2 : 0;
	return e->c;
}

char evolve(struct entry **board, int px, int py, int gen)
{
	int r = 0;
	for (int y = py-1; y<=py+1; y++)
		for (int x = px-1; x<=px+1; x++)
			r = r*2 + get_point(board, x, y, gen);
	return key[r];
}

int main()
{
	int c;
	int i = 0;
	while ((c = getchar()) != '\n')
		key[i++] = c == '#' ? 1 : 0;
	if (getchar() != '\n')
		return 1;

	struct entry *board = NULL;
	int x = 0, minx = 0, maxx = 0;
	int y = 0, miny = 0, maxy = 0;

	while ((c = getchar()) != EOF) {
		if (c == '\n') {
			if (x > maxx)
				maxx = x;
			x = 0;
			y++;
		} else {
			add_point(&board, x++, y, 0, c == '#' ? 1 : 0);
		}
	}
	maxy = y;

	int r = 0;
	for (int gen = 1; gen <= 50; gen++) {
		minx -= 3;
		miny -= 3;
		maxx += 3;
		maxy += 3;

		//Calculate r immediately all the time, saves 25ms
		r = 0;
		for (y = miny; y<maxy; y++) {
			for (x = minx; x<maxx; x++) {
				int nc = evolve(&board, x, y, gen-1);
				r += nc;
				add_point(&board, x, y, gen, nc);
			}
		}
		//Delete old items (saves ±1second, 160k instead of 3.6 elems)
		struct entry *e, *tmp;
		HASH_ITER(hh, board, e, tmp) {
			if (e->p.gen == gen-1)
				HASH_DEL(board, e);
			else
				break;
		}
	}

	printf("%d\n", r);
}
