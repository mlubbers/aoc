#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

bool parse_line(bool *on, int *x1, int *x2, int *y1, int *y2, int *z1, int *z2)
{
	char *buf = NULL;
	size_t len;
	if (getline(&buf, &len, stdin) == -1)
		return false;
	if (strncmp(buf, "on", 2) == 0) {
		*on = true;
		buf += 3;
	} else if (strncmp(buf, "off", 3) == 0) {
		*on = false;
		buf += 4;
	} else {
		return false;
	}
	sscanf(buf, "x=%d..%d,y=%d..%d,z=%d..%d", x1, x2, y1, y2, z1, z2);
	return true;
}

int main()
{
	int x1, x2, y1, y2, z1, z2;
	bool on;
	bool cube[100][100][100] = {0};
	while (parse_line(&on, &x1, &x2, &y1, &y2, &z1, &z2))
		for (int x = x1; x<=x2 && x>=-50 && x<=50; x++)
			for (int y = y1; y<=y2 && y>=-50 && y<=50; y++)
				for (int z = z1; z<=z2 && z>=-50 && z<=50; z++)
					cube[x+50][y+50][z+50] = on;

	int r = 0;
	for (int x = 0; x<100; x++)
		for (int y = 0; y<100; y++)
			for (int z = 0; z<100; z++)
				r += cube[x][y][z];
	printf("%d\n", r);
}

