#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>

int main()
{
	int grid[102][102];
	for (int i = 0; i<102; i++)
		for (int j = 0; j<102; j++)
			grid[i][j] = INT_MAX;
	int x = 1, y = 1;
	int maxx = 1;
	int c = 0;
	while((c = getchar()) != EOF) {
		if (c == '\n') {
			y++;
			x = 1;
		} else {
			maxx = x > maxx ? x : maxx;
			grid[y][x++] = c-'0';
		}
	}

	int r = 0;
	for (int i = 1; i<y; i++)
		for (int j = 1; j<maxx+1; j++)
			if (grid[i][j] < grid[i+1][j] &&
					grid[i][j] < grid[i-1][j] &&
					grid[i][j] < grid[i][j+1] &&
					grid[i][j] < grid[i][j-1])
				r += grid[i][j] + 1;
	printf("%d\n", r);

}
