#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>

bool read_int(int *r)
{
	char c;
	*r = 0;
	while (isdigit(c = getchar()))
		*r = *r*10 + c-'0';
	return c != EOF;
}

int one(int winsize)
{
	int win[winsize+1], wi = 0, increase = 0;
	for (; wi<winsize; wi++)
		read_int(&win[wi]);
	while (read_int(&win[wi])) {
		if (win[wi] - win[(wi+winsize+2) % (winsize+1)] > 0)
			increase++;
		wi = (wi+1) % (winsize+1);
	}
	return increase;
}

int main(void)
{
	printf("%d\n", one(1));
	return 0;
}
