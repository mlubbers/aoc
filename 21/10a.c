#include <stdio.h>

char cmap[] = { [')']='(', [']']='[', ['}']='{', ['>']='<' };
int score[] = { [')']=3, [']']=57, ['}']=1197, ['>']=25137 };

int parse_line(char *buf)
{
	char stack[100] = {0};
	int sp = 0;
	for (char *p = buf; *p != '\0'; p++) {
		switch (*p) {
		case '(':
		case '[':
		case '{':
		case '<':
			stack[sp++] = *p;
			break;
		case ')':
		case ']':
		case '}':
		case '>':
			//incomplete
			if (sp == 0)
				return 0;
			//corrupted
			if (stack[--sp] != cmap[(int)*p]) {
				return score[(int)*p];
			}
			stack[sp] = '\0';
			break;
		case '\n':
			break;
		}
	}
	return 0;
}

int main()
{
	char *buf = NULL;
	size_t len = 0;
	int r = 0;
	while (getline(&buf, &len, stdin) != -1)
		r += parse_line(buf);
	printf("%d\n", r);
}
