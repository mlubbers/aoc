#include <stdio.h>
#include <stdlib.h>

char cmap[] = { [')']='(', [']']='[', ['}']='{', ['>']='<' };
long rmap[] = { ['(']=1, ['[']=2, ['{']=3, ['<']=4 };

void parse_line(const char *buf, long scores[], int *nscores)
{
	char stack[200] = {0};
	int sp = 0;
	long score = 0;
	for (const char *p = buf; ; p++) {
		switch (*p) {
		case '(':
		case '[':
		case '{':
		case '<':
			stack[sp++] = *p;
			break;
		case ')':
		case ']':
		case '}':
		case '>':
			//corrupt
			if (stack[--sp] != cmap[(int)*p])
				return;
			stack[sp] = '\0';
			break;
		case '\0':
		case '\n':
			score = 0;
			while (sp>0)
				score = score*5 + rmap[(int)stack[--sp]];
			scores[(*nscores)++] = score;
			return;
		}
	}
}

int longcmp(const void *l, const void *r)
{
	return *(const long *)l-*(const long *)r < 0 ? -1 : 1;
}

int main()
{
	char *buf = NULL;
	size_t len = 0;
	long scores[100] = {0};
	int nscores = 0;
	while (getline(&buf, &len, stdin) != -1)
		parse_line(buf, scores, &nscores);
	qsort(scores, nscores, sizeof(long), &longcmp);
	printf("%ld\n", scores[nscores/2]);
}
