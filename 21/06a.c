#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	char *buf = NULL;
	size_t len = 0;
	long fish[9] = {0};

	getline(&buf, &len, stdin);
	char *p = strtok(buf, ",");
	fish[atoi(p)]++;
	while ((p = strtok(NULL, ",")) != NULL)
		fish[atoi(p)]++;

	for (long day = 0; day<80; day++)
		fish[(day+7) % 9] += fish[day%9];

	for (long i = 1; i<=8; i++)
		fish[0] += fish[i];
	printf("%ld\n", fish[0]);
}
