#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>
#include <uthash.h>

struct result { unsigned long p1; unsigned long p2; };
struct player { uint8_t pos; uint8_t score; };
struct key { struct player p1; struct player p2; bool p1turn; };
struct cache {
	struct key key;
	struct result res;
	UT_hash_handle hh;
};

struct player move (struct player p, int steps)
{
	p.pos = (p.pos+steps)%10;
	if (p.pos == 0)
		p.pos = 10;
	p.score += p.pos;
	return p;
}

struct cache *cache = NULL;

struct result playd (bool p1turn, struct player p1, struct player p2);
struct result play(bool p1turn, struct player p1, struct player p2)
{
	struct cache *e;
	struct key p;
	memset(&p, 0, sizeof(p));
	p = (struct key){.p1turn=p1turn, .p1=p1, .p2=p2};
	HASH_FIND(hh, cache, &p, sizeof(struct key), e);
	if (e == NULL) {
		struct result r = playd(p1turn, p1, p2);
		e = calloc(1, sizeof(struct cache));
		*e = (struct cache){.key=p, .res=r};
		HASH_ADD(hh, cache, key, sizeof(struct key), e);
		return r;
	} else {
		return e->res;
	}
}

static inline void add_result(struct result *l, struct result r)
{
	l->p1 += r.p1;
	l->p2 += r.p2;
}

#define FOR_THROWS(x) \
	for (int i = 1; i<=3; i++)\
		for (int j = 1; j<=3; j++)\
			for (int k = 1; k<=3; k++)\
				x

struct result playd (bool p1turn, struct player p1, struct player p2)
{
	struct result r = {.p1=0, .p2=0};
	if (p1.score >= 21)
		r.p1++;
	else if (p2.score >= 21)
		r.p2++;
	else if (p1turn)
		FOR_THROWS(add_result(&r, play(!p1turn, move(p1, i+j+k), p2)));
	else
		FOR_THROWS(add_result(&r, play(!p1turn, p1, move(p2, i+j+k))));
	return r;
}

int main()
{
	struct player p1 = {.score=0}, p2 = {.score=0};
	if (2 != scanf("Player 1 starting position: %" SCNu8 "\n"
		       "Player 2 starting position: %" SCNu8 "\n",
		       &p1.pos, &p2.pos))
		return 1;

	struct result r = play(true, p1, p2);
	printf("%lu\n", r.p1 > r.p2 ? r.p1 : r.p2);
}
