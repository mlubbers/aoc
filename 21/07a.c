#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

static inline int distance(int crabs[], int ncrabs, int pos)
{
	int dist = 0;
	for (int i = 0; i<ncrabs; i++)
		dist += abs(crabs[i]-pos);
	return dist;
}

int main()
{
	char *buf = NULL;
	size_t len = 0;
	int crabs[1000] = {0};
	int ncrabs = 0;
	int maxp = 0;
	int minp = INT_MAX;

	getline(&buf, &len, stdin);
	char *p = strtok(buf, ",");
	crabs[ncrabs] = atoi(p);
	maxp = crabs[ncrabs];
	minp = crabs[ncrabs++];
	while ((p = strtok(NULL, ",")) != NULL) {
		crabs[ncrabs] = atoi(p);
		maxp = maxp < crabs[ncrabs] ? crabs[ncrabs] : maxp;
		minp = minp > crabs[ncrabs] ? crabs[ncrabs] : minp;
		ncrabs++;
	}

	int middledist = INT_MAX;
	int middle = (maxp+minp)/2;
	while (middle != maxp && middle != minp) {
		middledist = distance(crabs, ncrabs, middle);
		if (distance(crabs, ncrabs, middle+1) > middledist) {
			maxp = middle;
			middle = (middle+minp)/2;
		} else {
			minp = middle;
			middle = (middle+maxp)/2;
		}
	}
	printf("%d\n", middledist);
}
