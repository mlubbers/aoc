#include <stdio.h>
#include <uthash.h>

#define SWAP(x, y) { x ^= y; y ^= x; x ^= y; }

struct point { int x; int y; };
struct entry { struct point key; int i; UT_hash_handle hh; };

void mark_point(int x, int y, struct entry **entries, int *r)
{
	struct entry *p;
	struct point s;
	memset(&s, 0, sizeof s);
	s.x = x;
	s.y = y;
	HASH_FIND(hh, *entries, &s, sizeof(struct point), p);
	if (p) {
		if (p->i++ == 1)
			*r = *r+1;
	} else {
		p = calloc(1, sizeof(struct entry));
		p->key = s;
		p->i = 1;
		HASH_ADD(hh, *entries, key, sizeof(struct point), p);
	}
}

int main()
{
	char buf[1000];
	struct entry *entries = NULL;
	int r = 0;
	while (fgets(buf, 1000, stdin) != NULL) {
		char *ptr = &buf[0];
		int x1 = strtol(ptr, &ptr, 10);
		ptr++;
		int y1 = strtol(ptr, &ptr, 10);
		ptr+=4;
		int x2 = strtol(ptr, &ptr, 10);
		ptr++;
		int y2 = strtol(ptr, &ptr, 10);

		//Diagonal
		if (abs(y2-y1) == abs(x2-x1)) {
			int dx = x1 > x2 ? -1 : 1;
			int dy = y1 > y2 ? -1 : 1;
			for (; x1 != x2; x1 += dx, y1+= dy)
				mark_point(x1, y1, &entries, &r);
			mark_point(x1, y1, &entries, &r);
		} else {
			if (x1 > x2)
				SWAP(x1, x2);
			if (y1 > y2)
				SWAP(y1, y2);
			//Vertical
			if (x1 == x2)
				for (int y = y1; y<=y2; y++)
					mark_point(x1, y, &entries, &r);
			//Horizontal
			else if (y1 == y2)
				for (int x = x1; x<=x2; x++)
					mark_point(x, y1, &entries, &r);
		}
	}
	printf("%d\n", r);
}
