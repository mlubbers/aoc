#include <stdio.h>

void move (int roll, int *p, int *score)
{
	*p = (*p+roll*9+6)%10;
	if (*p == 0)
		*p = 10;
	*score += *p;
}

int main()
{
	int p1, p2, p1score = 0, p2score = 0;
	if (2 != scanf(
			"Player 1 starting position: %d\n"
			"Player 2 starting position: %d\n", &p1, &p2))
		return 1;

	int roll = 0;
	while (p1score < 1000 && p2score < 1000) {
		if (roll%2==0)
			move(roll, &p1, &p1score);
		else
			move(roll, &p2, &p2score);
		roll++;
	}
	printf("%d\n", (roll%2==0 ? p1score : p2score)*roll*3);
}
