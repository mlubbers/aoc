module s2

import StdEnv
import Data.Func
import qualified Data.Set as DS
import Data.List
import Text

getinput :: [Char] *File -> [[Char]]
getinput a f
# (ok, c, f) = freadc f
| not ok = if (a =: []) [] [reverse a]
| c == '\n' = [reverse a:getinput [] f]
= getinput [c:a] f

parse :: [Char] -> (Int, [(Int, Int)])
parse c = case split " " (toString c) of
	[i,_, offset,size]
		# [offsetx:offsety:_] = split "," offset
		# offsety = offsety % (0, textSize offsety-2)
		# [sizex:sizey:_] = split "x" size
		= (toInt i, [ (x, y)
			\\x<-[toInt offsetx+1..toInt offsetx+toInt sizex]
			, y<-[toInt offsety+1..toInt offsety+toInt sizey]
			])
	_ = abort "bork"

Start w
# (io, w) = stdio w
= proc $ map parse $ getinput [] io

proc [x:xs] = p x xs
where
	p (i, cs) rest=:[x:xs]
		| 'DS'.null $ 'DS'.intersection ('DS'.fromList cs) $ 'DS'.fromList $ flatten $ map snd rest = i
		| not (trace_t ".") = undef
		= p x $ xs ++ [(i,cs)]
