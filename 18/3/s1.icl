module s1

import StdEnv
import Data.Func
import Data.List
import Text

getinput :: [Char] *File -> [[Char]]
getinput a f
# (ok, c, f) = freadc f
| not ok = if (a =: []) [] [reverse a]
| c == '\n' = [reverse a:getinput [] f]
= getinput [c:a] f

parse :: [Char] -> [(Int, Int)]
parse c = case split " " (toString c) of
	[i,_, offset,size]
		# [offsetx:offsety:_] = split "," offset
		# offsety = offsety % (0, textSize offsety-2)
		# [sizex:sizey:_] = split "x" size
		= [ (x, y)
			\\x<-[toInt offsetx+1..toInt offsetx+toInt sizex]
			, y<-[toInt offsety+1..toInt offsety+toInt sizey]
			]
	_ = abort "bork"

Start w
# (io, w) = stdio w
= length [c\\c<-group $ sort $ flatten $ map parse $ getinput [] io | length c > 1]
