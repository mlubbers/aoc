module s1

import StdEnv

import Data.Func
import Data.List

getinput f
# (ok, c, f) = freadc f
| not ok = []
= [c:getinput f]

split :: [Char] [Char] -> [[Char]]
split [] [] = []
split c [] = [reverse c]
split c ['\n':rest] = [reverse c:split [] rest]
split c [r:rest] = split [r:c] rest

Start w
# (io, w) = stdio w
# l = map (nub o map length o group o sort)
	$ split []
	$ getinput io
= ns 2 l * ns 3 l

ns n l = sum [1\\x<-l|isMember n x]
