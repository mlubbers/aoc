module s2

import StdEnv
import Data.Func
import Data.List

getinput :: [Char] *File -> [[Char]]
getinput a f
# (ok, c, f) = freadc f
| not ok = if (a =: []) [] [reverse a]
| c == '\n' = [reverse a:getinput [] f]
= getinput [c:a] f

Start w
# (io, w) = stdio w
= map (toString o map fst o filter (uncurry (==)) o zip)
	$ flatten
	$ filter (not o isEmpty)
	$ proc
	$ getinput [] io

proc [] = []
proc [x:xs] = [[(e, x)\\e<-xs | length [()\\a<-x & b<-e | a<>b] <= 1]:proc xs]
