module s2

import StdEnv

import Data.Func
import qualified Data.Set as DS

getinput f
# (ok, c, f) = freadc f
| not ok = []
= [c:getinput f]

split :: [Char] [Char] -> [[Char]]
split [] [] = []
split c [] = [reverse c]
split c ['\n':rest] = [reverse c:split [] rest]
split c [r:rest] = split [r:c] rest

Start w
# (io, w) = stdio w
= proc 'DS'.newSet
	$ scan (+) 0
	$ map (toInt o toString)
	$ flatten
	$ repeat
	$ split []
	$ getinput io

proc have [x:xs]
	| 'DS'.member x have = x
	= proc ('DS'.insert x have) xs
