module one

import StdEnv
import Data.Functor
import Data.Either
import Control.Applicative
import Text.Parsers.Simple.ParserCombinators

read :: *File -> [Char]
read f
	# (ok, l, f) = freadc f
	| not ok = []
	= [l:read f]

Start w
	# (io, w) = stdio w
	= case parse (many lineparser) (read io) of
		Left e = Left e
		Right a = Right (length (filter validone a), length (filter validtwo a))

validone :: (Int, Int, Char, String) -> Bool
validone (fro, to, a, s) = isMember (length [c\\c<-:s | c == a]) [fro..to]

validtwo :: (Int, Int, Char, String) -> Bool
validtwo (fro, to, a, s) = (s.[fro-1] == a || s.[to-1] == a) && s.[fro-1] <> s.[to-1]

lineparser :: Parser Char (Int, Int, Char, String)
lineparser = (\a b c d->(a, b, c, d)) <$> pInt <* pToken '-' <*> pInt <* pSpace <*> pAlpha <* pToken ':' <* pSpace <*> (toString <$> many pAlpha) <* pToken '\n'
where pInt = toInt o toString <$> some pDigit
