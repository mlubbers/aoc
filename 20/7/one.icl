module one

import StdEnv
import Data.Func
import Data.Map => qualified updateAt
import Text


//A bit cheaty because I removed all occurances of bag and bags from the input

read :: *File -> [Char]
read f
	# (ok, l, f) = freadc f
	| not ok = []
	= [l:read f]

Start w
	# (io, w) = stdio w
	# bag = fromList (makeBag (read io))
	= (one bag ['shiny gold'], dec $ two bag ['shiny gold'])

one bag color = dec $ length $ filter id $ map (canContain bag color) $ keys bag
two bag color = inc $ sum [n*two bag c\\(n, c)<-findWithDefault [] color bag]

makeBag :: [Char] -> [([Char], [(Int, [Char])])]
makeBag ls = [(b, mkItem c)\\[b,c]<-map (split [' contain ']) $ split ['.\n'] ls]
where
	mkItem :: [Char] -> [(Int, [Char])]
	mkItem s = [(toInt (toString x), join [' '] xs)\\[x:xs]<-map (split [' ']) $ split [', '] s]

canContain :: (Map [Char] [(Int,[Char])]) [Char] [Char] -> Bool
canContain bag color name
	= or [name == color:map (canContain bag color o snd) $ findWithDefault [] name bag]
