module one

import StdEnv
import qualified Data.Foldable
import Data.List
import Text

read :: *File -> [Char]
read f
	# (ok, l, f) = freadc f
	| not ok = []
	= [l:read f]

Start w
	# (io, w) = stdio w
	# ls = toString (read io)
	= (both union ls, both intersect ls)

both :: ([Char] -> [Char] -> [Char]) -> (String -> Int)
both f = sum o map (length o 'Data.Foldable'.foldl1 f o map fromString o split "\n" o trim) o split "\n\n"
