module one

import StdEnv, StdDebug
import Data.Either

input :: ([Int], [Int])
input =
	([45,10,43,46,25,36,16,38,30,15,26,34,9,2,44,1,4,40,5,24,49,3,41,19,13]
	,[28,50,37,20,6,42,32,47,39,22,14,7,21,17,27,8,48,11,23,12,18,35,29,33,31]
	)

//input :: ([Int], [Int])
//input =
//	([ 9, 2, 6, 3, 1]
//	,[ 5, 8, 4, 7, 10]
//	)

Start = (uncurry combat input, uncurry rcombat input [] [])

combat :: [Int] [Int] -> Either Int Int
combat [] b = Right (score b)
combat a [] = Left (score a)
combat [a:as] [b:bs]
	| a > b     = combat (as ++ [a,b]) bs
	| a < b     = combat as (bs ++ [b,a])
	| otherwise = abort "draw"

score :: [Int] -> Int
score d = sum [i*d\\d<-reverse d & i<-[1..]]

rcombat :: [Int] [Int] [[Int]] [[Int]] -> Either Int Int
rcombat [] b _ _ = Right (score b)
rcombat a [] _ _ = Left (score a)
rcombat [a:as] [b:bs] ahist bhist
	| isMember [a:as] ahist && isMember [b:bs] bhist = Left (score [a:as])
	# (ahist, bhist) = ([[a:as]:ahist], [[b:bs]:bhist])
	| a <= length as && b <= length bs
		= case rcombat (take a as) (take b bs) [] [] of
			Left s  = rcombat (as ++ [a,b]) bs ahist bhist
			Right s = rcombat as (bs ++ [b,a]) ahist bhist
	| a > b = rcombat (as ++ [a,b]) bs ahist bhist
	| a < b = rcombat as (bs ++ [b,a]) ahist bhist
