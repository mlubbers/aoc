module two

import StdEnv
import Data.Func
import Data.Tuple

read :: *File -> [(Char, Int)]
read f
	# (l, f) = freadline f
	| l.[size l - 1] <> '\n' = []
	= [(l.[0], toInt (l % (1, size l - 2))):read f]

Start w
	# (io, w) = stdio w
	# ls = read io
	= (\s->abs s.sx+abs s.sy)
	$ foldl move {sx=0,sy=0,wx=10,wy=1} ls

:: Ship = { sx :: Int, sy :: Int, wx :: Int, wy :: Int }

move s ('N', n) = {s & wy=s.wy+n}
move s ('S', n) = {s & wy=s.wy-n}
move s ('E', n) = {s & wx=s.wx+n}
move s ('W', n) = {s & wx=s.wx-n}
move s ('F', n)
	# dx = s.wx-s.sx
	# dy = s.wy-s.sy
	# s = {s & sx=s.sx+dx*n, sy=s.sy+dy*n}
	= {s & wx=s.sx+dx, wy=s.sy+dy}
move s ('R', n) = rot (n/90) s
move s ('L', n) = rot (3*(n/90)) s

//https://stackoverflow.com/questions/2259476/rotating-a-point-about-another-point-2d/25196651#25196651
rot :: Int Ship -> Ship
rot 0 ship = ship
rot n ship=:{sx, sy, wx, wy}
	# s = toInt (sin 90.0)
	# c = toInt (cos 90.0)
	# nx = sx + ( (wx-sx) * c + (wy-sy) * s)
	# ny = sy + (~(wx-sx) * s + (wy-sy) * c)
	= rot (n-1) {ship & wx=nx, wy=ny}
