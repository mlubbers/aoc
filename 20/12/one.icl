module one

import StdEnv
import Data.Func
import Data.Tuple

read :: *File -> [(Char, Int)]
read f
	# (l, f) = freadline f
	| l.[size l - 1] <> '\n' = []
	= [(l.[0], toInt (l % (1, size l - 2))):read f]

Start w
	# (io, w) = stdio w
	# ls = read io
	= (\s->abs s.x+abs s.y) $ foldl move {x=0,y=0,dirs=dirs} ls

dirs = [(1, 0),(0,-1),(-1,0),(0,1):dirs]

:: Ship = {x :: Int, y :: Int, dirs :: [(Int, Int)]}

move s ('N', n) = {s & y=s.y+n}
move s ('S', n) = {s & y=s.y-n}
move s ('E', n) = {s & x=s.x+n}
move s ('W', n) = {s & x=s.x-n}
move s=:{dirs=[(dx, dy):_]} ('F', n) = {s & x=s.x+dx*n, y=s.y+dy*n}
move s ('R', n) = {s & dirs=drop (n/90) s.dirs}
move s ('L', n) = {s & dirs=drop (3*(n/90)) s.dirs}
