module one

import StdEnv

read :: *File -> [Int]
read f
	# (ok, l, f) = freadi f
	| not ok = []
	= [l:read f]

Start w
	# (io, w) = stdio w
	# ls = read io
	= (one ls, two ls)

one :: [Int] -> Int
one nums = hd [x*y\\x<-nums, y<-nums | x + y == 2020]

two :: [Int] -> Int
two nums = hd [x*y*z\\x<-nums, y<-nums, z<-nums | x + y + z == 2020]
