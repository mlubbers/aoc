module one

import StdEnv
import Data.Func

inp :: (Int, [?Int])
inp = (1000067, [?Just 17,?None,?None,?None,?None,?None,?None,?None,?None,?None,?None,?Just 37,?None,?None,?None,?None,?None,?Just 439,?None,?Just 29,?None,?None,?None,?None,?None,?None,?None,?None,?None,?None,?Just 13,?None,?None,?None,?None,?None,?None,?None,?None,?None,?Just 23,?None,?None,?None,?None,?None,?None,?None,?Just 787,?None,?None,?None,?None,?None,?None,?None,?None,?None,?Just 41,?None,?None,?None,?None,?None,?None,?None,?None,?Just 19])

Start = (one inp, two inp)

one (cur, bs) = uncurry (*) $ hd $ sort [(b - cur rem b, b)\\(?Just b)<-bs]

two (start, busses)
	# b=:[(_, mbo):_] = sortBy (>) [(b, i)\\(?Just b)<-busses & i<-[0..]]
	= snd (foldl (crt mbo) (1, start) b) - mbo
where
	crt max_bus_offset (max_bus, startpoint) (next_biggest_bus, next_offset)
		# startpoint = while
			(\sp->(sp+next_offset-max_bus_offset) rem next_biggest_bus <> 0)
			(\sp->sp + max_bus)
			startpoint
		= (max_bus*next_biggest_bus, startpoint)
