module one

import StdEnv
import Text
import Data.Maybe
import Data.List
import Regex

read :: *File -> [String]
read f
	# (l, f) = freadline f
	| l.[size l - 1] == '\0' = []
	= [l % (0, size l - 2):read f]

group :: [(String, String)] [String] -> [[(String, String)]]
group acc [] = [acc]
group acc ["":xs] = [acc:group [] xs]
group acc [s:xs] = group (acc ++ [(k, v)\\[k,v]<-map (split ":") (split " " s)]) xs

valid :: [(String, String)] [(String, String)] -> Bool
valid req fs = all (\(f, p)->maybe False (not o isEmpty o match p) (lookup f fs)) req

Start w
	# (io, w) = stdio w
	# passports = group [] (read io)
	= (one passports, two passports)

one = length o filter (valid [(n, ".*")\\n<-["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]])
two = length o filter (valid
	[ ("byr", "^(19[2-9]\\d|200[0-2])$")
	, ("iyr", "^20(1\\d|20)$")
	, ("eyr", "^20(2\\d|30)$")
	, ("hgt", "^(1([5-8]\\d|9[0-3])cm|(59|6\\d|7[0-6])in)$")
	, ("hcl", "^#[0-9a-f]{6}$")
	, ("ecl", "^(amb|blu|brn|gry|grn|hzl|oth)$")
	, ("pid", "^\\d{9}$")
	])
