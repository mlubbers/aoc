module one

import StdEnv

read :: *File -> [Instr]
read f
	# (l, f) = freadline f
	| l.[size l - 1] <> '\n' = []
	= [toInstr [c\\c<-:l | c <> '\n']:read f]
where
	toInstr ['nop ':rs] = Nop (toInt (toString rs))
	toInstr ['acc ':rs] = Acc (toInt (toString rs))
	toInstr ['jmp ':rs] = Jmp (toInt (toString rs))

:: Instr = Nop Int | Acc Int | Jmp Int

Start w
	# (io, w) = stdio w
	# instr = read io
	# instr = {c\\c<-instr}
	= (one instr, two instr)

one = fst o interpret
two is = [ acc \\i<-:is & pc<-[0..]
	, let (acc, t) = interpret (update {i\\i<-:is} pc (swapnop i))
	| t]
where
	swapnop (Nop i) = Jmp i
	swapnop (Jmp i) = Nop i
	swapnop i = i

interpret :: {Instr} -> (Int, Bool)
interpret instr = int [] 0 0
where
	int vis pc acc
		| pc >= size instr = (acc, True) // terminated
		| isMember pc vis = (acc, False) // inf loop
		= case instr.[pc] of
			Nop _ = int [pc:vis] (1 + pc) acc
			Acc i = int [pc:vis] (1 + pc) (i + acc)
			Jmp i = int [pc:vis] (i + pc) acc
