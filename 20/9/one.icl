module one

import StdEnv
import Data.List
import Data.Func
import StdMaybe

read :: *File -> [Int]
read f
	# (ok, i, f) = freadi f
	| not ok = []
	= [i:read f]

Start w
	# (io, w) = stdio w
	# ls = read io
	= (one ls, two ls)

one = last o pre

pre l
	# (preamble, [num:rest]) = splitAt 25 l
	| isEmpty [()\\(x, y)<-diag2 preamble preamble | x+y==num] = [num]
	= [num:pre (tl l)]

two ls
	# ls = fst (splitAt 25 ls) ++ pre ls
	= let a = calc (last ls) ls in last a + hd a
where
	calc num = sort o hd o hd o filter (not o isEmpty) o map (filter ((==)num o sum) o dropWhile ((<)num o sum) o inits) o tails
