module one

import StdEnv

read :: *File -> [[Int]]
read f
	# (l, f) = freadline f
	| l.[size l - 1] <> '\n' = []
	= [let c = [if (c == '#') 1 0\\c<-:l | c <> '\n'] ++ c in c:read f]

Start w
	# (io, w) = stdio w
	# ls = read io
	= (one ls, two ls)

one ls :== slope ls (3, 1)

two :: [[Int]] -> Int
two ls = prod (map (slope ls) [(1,1), (3,1), (5,1), (7,1), (1,2)])

slope :: [[Int]] (Int, Int) -> Int
slope [] _ = 0
slope xs=:[[c:_]:_] d=:(right, down) = c + slope (map (drop right) (drop down xs)) d
