module one

import StdEnv

read :: *File -> [[Int]]
read f
	# (l, f) = freadline f
	| l.[size l - 1] <> '\n' = []
	= [[tobin c\\c<-:l % (0, size l - 2)]:read f]
where
	tobin 'B' = 1
	tobin 'F' = 0
	tobin 'R' = 1
	tobin 'L' = 0

Start w
	# (io, w) = stdio w
	# ls = read io
	= (one ls, findHole (sort (map seatNo ls)))

findHole x = hd [b+1\\a<-x & b<-tl x & c<-tl (tl x) | a+1 == b && b+1 <> c]
one = foldl (\m v->if (v>m) v m) 0 o map seatNo
seatNo = foldl (\n s->s + 2*n) 0
