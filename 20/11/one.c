#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

enum cellstatus {floor, empty, occup, oob};

uint16_t map1[100][100] = {oob};
uint16_t map2[100][100] = {oob};
int xmax, ymax;

enum cellstatus seat(int x, int y, uint16_t map[100][100])
{
	return x < 0 || y < 0 || x >= xmax || y >= ymax ? oob
		: (enum cellstatus)((map[x][y] >> 8) & 0xff);
}

bool lookone(int x, int y, int dx, int dy, uint16_t map[100][100])
{
	return seat(x+dx, y+dy, map) == occup;
}

bool looktwo(int x, int y, int dx, int dy, uint16_t map[100][100])
{
	enum cellstatus s;
	while((s = seat(x += dx, y += dy, map)) == floor);
	return s == occup;
}

bool step(bool (*lookfun)(int, int, int, int, uint16_t map[100][100]), int die, uint16_t map[100][100])
{
	//Move the old to the leftmost byte
	for (int y = 0; y<ymax; y++)
		for (int x = 0; x<xmax; x++)
			map[x][y] <<= 8;
	//Do a step
	bool same = true;
	for (int y = 0; y<ymax; y++) {
		for (int x = 0; x<xmax; x++) {
			enum cellstatus st = seat(x, y, map);
			if (st == floor) {
				map[x][y] |= floor;
				continue;
			}
			int count = 0;
			for (int dx = -1; dx<=1; dx++) {
				for (int dy = -1; dy<=1; dy++) {
					if (dx == 0 && dy == 0)
						continue;
					if (lookfun(x, y, dx, dy, map))
						count++;
				}
			}
			if (st == empty) {
				if (count == 0) {
					same = false;
					map[x][y] |= occup;
				} else {
					map[x][y] |= empty;
				}
			} else if(st == occup) {
				if (count >= die) {
					same = false;
					map[x][y] |= empty;
				} else {
					map[x][y] |= occup;
				}
			}
		}
	}
	return same;
}

int main()
{
	int x = 0, y = 0;
	int c;
	while ((c = getchar()) != EOF) {
		switch (c) {
		case 'L':
			map1[x][y] = empty;
			map2[x++][y] = empty;
			break;
		case '.':
			map1[x][y] = floor;
			map2[x++][y] = floor;
			break;
		case '\n':
			xmax = x;
			x = 0;
			y++;
			break;
		}
	}
	ymax = y;

	//Do one
	while(!step(lookone, 4, map1));

	//Do two
	while(!step(looktwo, 5, map2));

	int count1 = 0, count2 = 0;
	for (int y = 0; y<ymax; y++) {
		for (int x = 0; x<xmax; x++) {
			if ((map1[x][y] & 0xff) == occup)
				count1++;
			if ((map2[x][y] & 0xff) == occup)
				count2++;
		}
	}
	printf("one: %d, two: %d\n", count1, count2);
	return 0;
}
