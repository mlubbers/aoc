module one

import StdEnv

import Data.List
import Data.Func

read :: *File -> [Int]
read f
	# (ok, i, f) = freadi f
	| not ok = []
	= [i:read f]

Start w
	# (io, w) = stdio w
	# ls = sort [0:read io]
	= (one ls, two ls)

one ls = prod $ map length $ group $ sort [3:[b-a\\a<-ls & b<-tl ls]]

two ls = prod [blurp !! length l\\l<-group [b-a\\a<-ls & b<-tl ls] | all ((==)1) l]

blurp =: let seq a b = [a+b-1:seq b (a+b)] in seq 0 1
